// Glavit / Главлит: General Directorate for the Protection of State Secrets in the Press 
//    under the Council of Ministers of the USSR (Russian: Главное управление по охране 
//    государственных тайн в печати при СМ СССР) was the official censorship and 
//    state secret protection organ in the Soviet Union.
//
// Original code by Rigel Kent, 2019 (https://framagit.org/rigelk/peertube-plugin-glavlit)
// Heavily modified by Peter "grin" Gervai, 2020
// 

const mm = require("micromatch")
const dnsbl = require("dnsbl")

async function register({
  registerHook,
  peertubeHelpers,
  registerSetting,
  settingsManager
}) {
  registerSetting({
    name: "registration-blocked-email-right",
    label:
      "Registration: blocked email right part (domain) list, comma-separated",
    type: "input-textarea",
    private: true
  })
  registerSetting({
    name: "registration-blocked-email-left",
    label:
      "Registration: blocked email left part (local part) list, comma-separated",
    type: "input-textarea",
    private: true
  })
  registerSetting({
    name: "registration-dns-email-blacklists",
    label: "Registration: <a href='https://en.wikipedia.org/wiki/DNSBL'>DNS-based blackhole lists</a>, comma-separated",
    type: "input-textarea",
    private: true
  })
  registerSetting({
    name: "video-autoblacklist",
    label:
      "Video update and creation: <a href='https://github.com/micromatch/micromatch'>text rules</a> that put videos on blacklist automatically, comma-separated",
    type: "input-textarea",
    private: true
  })
  registerSetting({
    name: "comment-autohide",
    label:
      "Comments under videos: <a href='https://github.com/micromatch/micromatch'>text rules</a> that hides comments automatically, comma-separated",
    type: "input-textarea",
    private: true
  })

  registerHook({
    target: "filter:api.user.signup.allowed.result",
    handler: (result, params) =>
      filterRegistration(result, params, settingsManager, peertubeHelpers)
  })
  registerHook({
    target: "filter:video.auto-blacklist.result",
    handler: (result, params) =>
      autoblacklistVideos(result, params, settingsManager, peertubeHelpers)
  })
  registerHook({
    target: "filter:api.video.upload.accept.result",
    handler: (result, params) =>
      autoblacklistVideos(result, params, settingsManager, peertubeHelpers)
  })
  registerHook({
    target: "filter:api.video-thread-comments.list.result",
    handler: (result, params) =>
      autohideThreads(result, params, settingsManager, peertubeHelpers)
  })
}

async function unregister() {
  return
}

module.exports = {
  register,
  unregister
}

// logging is done by winston; error, warn, info, http, verbose, debug, silly

async function filterRegistration(result, params, settingsManager,peertubeHelpers) {
  // params: { body, ip }

  const { logger } = peertubeHelpers

  if (!params || !params.body || !params.ip) {
    logger.verbose('glavliiit filterRegistration called with empty params')
    return result; 
  }

  // check right part of the mail address for forbidden domains, if any
  const blockedEmailRight = (
    await settingsManager.getSetting("registration-blocked-email-right")
  )
    .replace(/\s/g, "")
    .split(",");
  if(params.body && params.body.email && blockedEmailRight)
   if (mm.isMatch(params.body.email.split("@")[1], blockedEmailRight)) 
    return { allowed: false }

  // check left part of the mail address for forbidden username, if any
  const blockedEmailLeft = (
    await settingsManager.getSetting("registration-blocked-email-left")
  )
    .replace(/\s/g, "")
    .split(",");
  if(params.body && params.body.email && blockedEmailLeft)
   if (mm.isMatch(params.body.email.split("@")[0], blockedEmailLeft))
    return { allowed: false }

  // check IP against known blacklists, if any
  const blacklists = await settingsManager.getSetting(
    "registration-dns-email-blacklists"
  )
  if (blacklists) {
    if (!params.ip) return { allowed: false }
    const res = await dnsbl.batch(
      [params.ip],
      blacklists.replace(/\s/g, "").split(",")
    );
    if (m = res.find(ip => ip.listed)) {
      logger.warn("RBL match " + params.ip + " at " + m["blacklist"])
      return { allowed: false }
    }
  }

  logger.verbose('glavliiit found no reason to block registration of '+params.body.email+' from '+params.ip)
  return result
}

async function autoblacklistVideos(result, params, settingsManager,peertubeHelpers) {
  // params: { video, user, isRemote, isNew }
  // return true to blacklist, result not to
  //console.log(params.video)

  const { logger } = peertubeHelpers
  
  if (!params || !params.video) {
    logger.verbose('glavliiit autoblacklistVideos called with empty params')
    return result
  }

  logger.verbose("glavliiit is probing video "+params.video.dataValues['channelId']+"/"+params.video.dataValues['name']+".")

  const videoRules = (await settingsManager.getSetting("video-autoblacklist"))
    .replace(/\s/g, "")
    .split(",");
  if (!videoRules) return result;

  const checkedFields = [ "name", "description", "support" ];
  if (checkedFields.some(field => {
        if (params.video.dataValues[field]) {
          const matching = mm( params.video.dataValues[field], videoRules, { contains: true } )
          if (matching.length >0) {
            logger.warn("glavliiit blocked "+params.video.dataValues['channelId']+"/"+params.video.dataValues['name']+" on " + field + ": " + matching)
            return true
          }
        }
      })) {
    return { allowed: false }
  }

  if (params.video.dataValues.Tags)
    if (mm.some(params.video.dataValues.Tags, videoRules)) {
      logger.warn("glavliiit blocked "+params.video.dataValues['channelId']+"/"+params.video.dataValues['name']+" on Tags: " + params.video.dataValues.Tags)
      return { allowed: false }
    }

  logger.verbose("glavliiit found no reason to block video "+params.video.dataValues['channelId']+"/"+params.video.dataValues['name']+" :-)")
  return result
}

async function autohideThreads(result, params, settingsManager,peertubeHelpers) {
  // params: { thread }
  // return true to blacklist, result not to
  //console.log(params.threadg)

  if (!params || !params.thread || !params.thread.comment) return result

  const commentRules = (await settingsManager.getSetting("comment-autohide"))
    .relpace(/\s/g, "")
    .split(",");
  if (commentRules && params.thread.comment)
    if (mm.contains(params.thread.comment, commentRules)) {
      logger.warn("glavliiit is blocking comment on video "+params.video.dataValues['channelId']+"/"+params.video.dataValues['name']+": "+params.thread.comment)
      return { allowed: false }
    }

  return result
}
