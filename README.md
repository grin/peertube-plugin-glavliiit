# Glavliiit

Enhanced moderation tools for PeerTube. This one is the fork of 
[Glavlit](https://framagit.org/rigelk/peertube-plugin-glavlit) to fix all
the pending bugs.

## Read First

This plugin is NOT supported by Framasoft, nor any organisation.

This plugin wouldn't be possible without the support of Framasoft and the PeerTube developers
in making plugin APIs tailored for flexible filtering of all things a moderation plugin needs.

## Features

- registration:
  - block email domains (exact, glob)
  - block email left parts (exact, glob)
  - block email via DNS-based blacklists
- videos:
  - put new/updated videos containing known words/glob on blacklist

All fields are comma (",") separated [micromatches](https://github.com/micromatch/micromatch), 
supporting shell-like globs as well as "almost regexp like" matching.


## Why here and not in mainline PeerTube?

- it is easier to use the plugin API than find your way in the codebase of PeerTube (lowers the bar for new developers only interested in moderation)
- it allows to experiment with strategies that could be resource-consuming or using third-party services
- it allows to go faster than PeerTube mainline at deploying changes, a requirement for some moderation strategies
